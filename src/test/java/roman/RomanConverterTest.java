package roman;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RomanConverterTest extends RomanConverter {

	/*
	  
	// Interface-based Approach
	
	@Test
	public void fromRoman_string_length_0() {
		RomanConverter rc = new RomanConverter();
		String s = "";
		rc.fromRoman(s);
	}
		
	@Test
	public void fromRoman_string_length_1() {
		RomanConverter rc = new RomanConverter();
		String s = "v";
		rc.fromRoman(s);
	}

	@Test
	public void fromRoman_string_length_10() {
		RomanConverter rc = new RomanConverter();
		String s = "aaaaaaaaaa";
		rc.fromRoman(s);
	}

	@Test
	public void fromRoman_string_lowercase() {
		RomanConverter rc = new RomanConverter();
		String s = "x";
		rc.fromRoman(s);
	}

	@Test
	public void fromRoman_string_uppercase() {
		RomanConverter rc = new RomanConverter();
		String s = "X";
		rc.fromRoman(s);
	}

	@Test
	public void fromRoman_utf8() {
		RomanConverter rc = new RomanConverter();
		// Unicode for V
		String s = "\u0056";
		rc.fromRoman(s);
	}

	@Test
	public void fromRoman_space() {
		RomanConverter rc = new RomanConverter();
		String s = " ";
		rc.fromRoman(s);
	}
	
	
	@Test
	public void toRoman_negativeInt() {
		RomanConverter rc = new RomanConverter();
		int n = -1;
		rc.toRoman(n);
	}
	
	@Test
	public void toRoman_Zero() {
		RomanConverter rc = new RomanConverter();
		int n = 0;
		rc.toRoman(n);
	}
	
	@Test
	public void toRoman_One() {
		RomanConverter rc = new RomanConverter();
		int n = 1;
		rc.toRoman(n);
	}
	
	@Test
	public void toRoman_LargePositive() {
		RomanConverter rc = new RomanConverter();
		int n = 10000;
		rc.toRoman(n);
	}
	
	@Test
	public void toRoman_LargeNegative() {
		RomanConverter rc = new RomanConverter();
		int n = -10000;
		rc.toRoman(n);
	}
	
	*/
	
	//Functionality-based Approach

	@Test
	public void fromRoman_ValidLetterI() {
		RomanConverter c = new RomanConverter();
		String s = "I";
		assertEquals(1, c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_ValidLetterV() {
		RomanConverter c = new RomanConverter();
		String s = "V";
		assertEquals(5, c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_ValidLetterX() {
		RomanConverter c = new RomanConverter();
		String s = "X";
		assertEquals(10, c.fromRoman(s));
		
	}
	
	@Test
	public void fromRoman_ValidLetterC() {
		RomanConverter c = new RomanConverter();
		String s = "C";
		assertEquals(100, c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_ValidLetterD() {
		RomanConverter c = new RomanConverter();
		String s = "D";
		assertEquals(500, c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_ValidLetterM() {
		RomanConverter c = new RomanConverter();
		String s = "M";
		assertEquals(1000, c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_ValidLetterL() {
		RomanConverter c = new RomanConverter();
		String s = "L";
		assertEquals(50, c.fromRoman(s));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_InvalidLetterA() {
		RomanConverter c = new RomanConverter();
		String s = "A";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseI() {
		RomanConverter c = new RomanConverter();
		String s = "i";
		c.fromRoman(s);	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseV() {
		RomanConverter c = new RomanConverter();
		String s = "v";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseX() {
		RomanConverter c = new RomanConverter();
		String s = "x";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseC() {
		RomanConverter c = new RomanConverter();
		String s = "c";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseD() {
		RomanConverter c = new RomanConverter();
		String s = "d";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseM() {
		RomanConverter c = new RomanConverter();
		String s = "m";
		c.fromRoman(s);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidLowercaseL() {
		RomanConverter c = new RomanConverter();
		String s = "l";
		c.fromRoman(s);
	}
	
	@Test
	public void fromRoman_validRoman() {
		RomanConverter c = new RomanConverter();
		String s = "XII";
		assertEquals(12,c.fromRoman(s));
	}
	
	@Test
	public void fromRoman_validHigh() {
		RomanConverter c = new RomanConverter();
		String s = "MMMCMXCIX";
		assertEquals(3999,c.fromRoman(s));	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRoman_invalidRoman() {
		RomanConverter c = new RomanConverter();
		String s = "IIX";
		c.fromRoman(s);	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void toRoman_invalidLow() {
		RomanConverter c = new RomanConverter();
		int i = 0;
		c.toRoman(i);	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void toRoman_invalidNegative() {
		RomanConverter c = new RomanConverter();
		int i = -1;
		c.toRoman(i);	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void toRoman_invalidHigh() {
		RomanConverter c = new RomanConverter();
		int i = 4000;
		c.toRoman(i);	
	}
	
	@Test
	public void toRoman_validLow() {
		RomanConverter c = new RomanConverter();
		int i = 1;
		c.toRoman(i);	
	}
	
	@Test
	public void toRoman_validHigh() {
		RomanConverter c = new RomanConverter();
		int i = 3999;
		c.toRoman(i);	
	}
	
	@Test
    public void reversibleAllInRange() {
    		RomanConverter rc = new RomanConverter();
    		String romanStr;
    		int romanInt;
    		for (int i=1; i < 4000; ++i) {
    			romanStr = rc.toRoman(i);
    			romanInt = rc.fromRoman(romanStr);
    			assertEquals(i, romanInt);
    		}
    }
	
	@Test
	public void toRoman_invalidDouble() {
		RomanConverter c = new RomanConverter();
		double d = 2.0;
		// c.toRoman(d);	
	}
	
}

